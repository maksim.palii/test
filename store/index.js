import Cookies from 'js-cookie'

function getCookieBannerValue() {
  return Cookies.get('izneo_cookie_banner')
    ? Cookies.get('izneo_cookie_banner') !== 'hidden'
    : localStorage.getItem('izneo_cookie_banner') !== 'hidden'
}

function getModalCookie(modal) {
  return Cookies.get(modal)
    ? Cookies.get(modal) !== 'hidden'
    : localStorage.getItem(modal) !== 'hidden'
}

export const state = () => ({
  loadedFromLocalStorage: false,
  userDataFetched: false,

  showIzneoCookieBanner: process.browser ? getCookieBannerValue() : false,

  sessionCookieName: null,
  sessionCookieVal: null,

  displayPreferenceModal: process.browser
    ? getModalCookie('displayOnboardingModal')
    : false,
  displayAlbumGiftModal: process.browser
    ? getModalCookie('displayAlbumGiftModal')
    : false,

  cartItemCount: 0,

  homepageData: null,
  serverPartnerInfo: null
})

export const mutations = {
  UPDATE_STATE_FROM_LOCAL_STORAGE(state) {
    state.showIzneoCookieBanner = getCookieBannerValue()
    state.displayPreferenceModal = getModalCookie('displayOnboardingModal')
    state.displayAlbumGiftModal = getModalCookie('displayAlbumGiftModal')
    state.loadedFromLocalStorage = true
  },

  UPDATE_USER_DATA_FETCHED(state, val) {
    state.userDataFetched = val
  },

  SET_COOKIE_BANNER_HIDDEN(state, domain) {
    state.showIzneoCookieBanner = false

    if (process.browser) {
      localStorage.setItem('izneo_cookie_banner', 'hidden')
      Cookies.set('izneo_cookie_banner', 'hidden', {
        expires: 365,
        path: '/',
        domain: `.${domain}`
      })
    }
  },

  UPDATE_COOKIE(state, cookie) {
    state.sessionCookieName = cookie.name
    state.sessionCookieVal = cookie.val
  },

  SET_CART_ITEM_COUNT(state, count) {
    state.cartItemCount = count
  },

  UPDATE_COOKIE_MODAL(state, modal) {
    let cookieName
    if (modal === 'preference') {
      state.displayPreferenceModal = false
      cookieName = 'displayOnboardingModal'
    }

    if (modal === 'album-gift') {
      state.displayAlbumGiftModal = false
      cookieName = 'displayAlbumGiftModal'
    }

    if (process.browser) {
      localStorage.setItem(cookieName, 'hidden')
      Cookies.set(cookieName, 'hidden', {
        expires: 365,
        path: '/',
        domain: `.${this.$config.izneoDomain}`
      })
    }
  },

  SET_HOMEPAGE_DATA(state, data) {
    state.homepageData = data
  },

  SET_PARTNER_COOKIE_DATA(state, data) {
    state.serverPartnerInfo = data
  },

  SET_PARTNER_COOKIE(state) {
    if (
      process.browser &&
      state.serverPartnerInfo &&
      state.serverPartnerInfo.partner
    ) {
      Cookies.set('pId', state.serverPartnerInfo.partner, {
        expires: state.serverPartnerInfo.expiration
          ? Math.floor(state.serverPartnerInfo.expiration / (24 * 60 * 60))
          : 2,
        path: '/',
        domain: `.${this.$config.izneoDomain}`,
        sameSite: 'Lax',
        secure: true
      })

      state.serverPartnerInfo = null
    }
  }
}

export const actions = {
  // runs on server side-only!
  // save site cookie in the store
  nuxtServerInit({ dispatch }, { app }) {
    const cookiesReq = app.$cookies.getAll()
    dispatch('setRequestCookies', cookiesReq)
    // dispatch('updateHomepageData', {
    //   force: true,
    //   subArea: false,
    //   locale: app.i18n.locale
    // })
  },

  // called in plugin
  nuxtClientInit({ state, commit, dispatch }) {
    if (!state.loadedFromLocalStorage) {
      commit('UPDATE_STATE_FROM_LOCAL_STORAGE')
    }
    // remove token from localStorage, it's not needed anymore because it's cookie time!
    if (localStorage.getItem('auth._token.local')) {
      localStorage.removeItem('auth._token.local')
    }

    dispatch('checkAffiliation')
  },

  checkLogout() {
    if (localStorage.getItem('user.logout')) {
      this.$auth.setToken('local', null)
      this.$auth.$storage.setState('loggedIn', false)
      this.$auth.$storage.setLocalStorage('token', null)
      this.$auth.logout()
      localStorage.removeItem('user.logout')
    }
  },

  doLogout() {
    // it's necessary to set user.logout here! don't remove
    localStorage.setItem('user.logout', 1)
    this.$auth.setToken('local', null)
    this.$auth.$storage.setState('loggedIn', false)
    this.$auth.$storage.setLocalStorage('token', null)
    this.$auth.logout()
  },

  hideCookieBanner({ commit }) {
    commit('SET_COOKIE_BANNER_HIDDEN', this.$config.izneoDomain)
  },

  hideCookieModal({ commit }, modal) {
    commit('UPDATE_COOKIE_MODAL', modal)
  },

  setRequestCookies({ commit }, param) {
    let sessKey = '',
      sessId = ''

    for (const key in param) {
      if (key === 'lang') {
        //lang = param[key]
      } else {
        if (
          key.match(/^[0-9a-f]{32}$/i) &&
          param[key].match(/^[0-9a-z]{16,}$/i)
        ) {
          sessKey = key
          sessId = param[key]
        }
      }
    }

    commit('UPDATE_COOKIE', { name: sessKey, val: sessId })
  },

  async requestToken({ state }) {
    if (!this.$auth.token) {
      try {
        const data = await this.$api.getToken(
          state.sessionCookieName,
          state.sessionCookieVal
        )

        if (data) {
          await this.$auth.setUserToken(data.token)
        } else {
          this.$auth.setToken('local', null)
          this.$auth.$storage.setState('loggedIn', false)
          this.$auth.$storage.setLocalStorage('token', null)
          this.$axios.setToken(null)

          this.$auth.logout()
        }
      } catch (e) {
        this.$auth.setToken('local', null)
        this.$auth.$storage.setState('loggedIn', false)
        this.$auth.$storage.setLocalStorage('token', null)
        this.$axios.setToken(null)
        this.$auth.logout()
      }
    }
  },

  async updateCartItemCount({ commit }) {
    if (this.$auth.loggedIn && this.$auth.user) {
      const count = await this.$api.getCartCount()
      if (count) {
        commit('SET_CART_ITEM_COUNT', count)
      } else {
        commit('SET_CART_ITEM_COUNT', 0)
      }
    } else {
      commit('SET_CART_ITEM_COUNT', 0)
    }
  },

  async updateHomepageData({ state, commit }, { force, subArea, locale }) {
    if (state.homepageData && !force) return

    let homepageData = null

    const data = await this.$api.home(subArea, locale || null)
    if (data) {
      // get datas from API and rework them in a section collection
      const selections = []
      const sections = data.map((section, index) => {
        let dt

        if (section.type === 'selection') {
          dt = {
            title: section.title,
            reference: section.reference,
            shelf: section.shelf,
            itemId: section.itemId,
            series: section.data
          }
          selections.push(`${section.reference}-${index}`)
        } else {
          dt = {
            title: section.title,
            items: section.data
          }
        }
        return {
          type: section.type,
          data: dt
        }
      })

      homepageData = { selections, sections }
    }

    commit('SET_HOMEPAGE_DATA', homepageData)
  },

  resetHomepageData({ commit }) {
    commit('SET_HOMEPAGE_DATA', null)
  },

  async updateUserData({ dispatch, state, commit }) {
    await dispatch('checkLogout')

    if (!state.loadedFromLocalStorage) {
      commit('UPDATE_STATE_FROM_LOCAL_STORAGE')
    }

    if (this.$auth.loggedIn && this.$auth.user) {
      commit('UPDATE_USER_DATA_FETCHED', true)
      dispatch('updateCartItemCount')
      return
    }

    const token = this.$auth.token

    try {
      if (token) {
        // something went wrong, request token again
        await this.$auth.setUserToken(null)
      }

      await dispatch('requestToken')
      dispatch('updateCartItemCount')
    } catch (e) {
      this.$auth.setToken('local', null)
      this.$auth.$storage.setState('loggedIn', false)
      this.$auth.$storage.setLocalStorage('token', null)
      this.$auth.logout()
    }

    commit('UPDATE_USER_DATA_FETCHED', true)
  },

  async refreshUserData({ dispatch, commit }, route) {
    if (process.client) {
      if (!state.loadedFromLocalStorage) {
        commit('UPDATE_STATE_FROM_LOCAL_STORAGE')
      }
      await dispatch('checkLogout')
    }

    if (this.$auth.loggedIn && this.$auth.user) {
      commit('UPDATE_USER_DATA_FETCHED', true)
      dispatch('updateCartItemCount')
      return
    }

    const userData = await this.$api.getUserData()
    this.$auth.setUser(userData)

    if (userData) {
      await dispatch('updateCartItemCount')
    } else {
      // check and request partner cookie data for guests
      // (for affiliation)
      await dispatch('getAffiliation', route)
    }
  },

  async getAffiliation({ commit }, route) {
    if (route.query.partner) {
      const data = await this.$api.getPartnerCookieData(route.query.partner)
      if (data.partner) {
        commit('SET_PARTNER_COOKIE_DATA', data)
        commit('SET_PARTNER_COOKIE')
      }
    }
  },

  async checkAffiliation({ commit }) {
    commit('SET_PARTNER_COOKIE')
  }
}
