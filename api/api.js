import Axios from 'axios'

export default ($config, $axios, $i18n) => ({
  __l(loc) {
    return loc === undefined ? $i18n.locale : loc
  },

  extractIdFromSlugId(val) {
    const r = val.match(/^(.*)-(\d+)$/)
    const id = r.length === 3 ? r[2] : val
    const slug = r.length === 3 ? r[1] : val
    return { id, slug }
  },

  async __get(url, type, locale) {
    try {
      console.log(`/${this.__l(locale)}${url}`)
      const { data } = await $axios.get(`/${this.__l(locale)}${url}`)

      return data
    } catch (e) {
      console.log(`Error loading ${type ? type : ''}: ${e.message}`)
    }
    return null
  },

  async __postForm(url, type, formData, locale, addLocale = true) {
    try {
      const exactUrl = addLocale ? `/${this.__l(locale)}${url}` : url
      const { data } = await $axios.post(exactUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })

      return data
    } catch (e) {
      console.log(`Error posting form ${type ? type : ''}: ${e.message}`)
    }
    return null
  },

  async __post(url, type, dataToSend, locale, addLocale = true) {
    try {
      const exactUrl = addLocale ? `/${this.__l(locale)}${url}` : url
      const { data } = await $axios.post(exactUrl, dataToSend)

      return data
    } catch (e) {
      console.log(`Error posting data ${type ? type : ''}: ${e.message}`)
    }
    return null
  },

  async publishers(offset, locale) {
    return this.__get(`/api/web/publishers/${offset}`, 'publishers', locale)
  },

  async publisher(id, locale) {
    return this.__get(`/api/web/publisher/${id}`, 'publisher', locale)
  },

  async publisherSeries(id, offset, locale) {
    return this.__get(
      `/api/web/publisher/${id}/series/${offset}`,
      'publisher series',
      locale
    )
  },

  async author(id, locale) {
    return this.__get(`/api/web/author/${id}`, 'author', locale)
  },

  async authorAlbums(id, offset, locale) {
    return this.__get(
      `/api/web/author/${id}/albums/${offset}`,
      'author albums',
      locale
    )
  },

  async subscriptionMagazines(locale) {
    return this.__get(`/api/magazine/all`, 'subscription magazines', locale)
  },

  async profil(id, locale) {
    return this.__get(`/api/profil/${id}`, 'profil', locale)
  },

  async postAvatarForm(formData, locale) {
    return this.__postForm('/profil_upload_avatar', 'avatar', formData, locale)
  },

  async postBio(bio, locale) {
    return this.__post('/profil_update_bio', 'bio', { message: bio }, locale)
  },

  async serie(id, locale) {
    return this.__get(`/api/web/serie/${id}`, 'serie', locale)
  },

  async serieAlbums(id, type, order, offset, limit, locale) {
    return this.__get(
      `/api/web/serie/${id}/${type}/${order}/${offset}/${limit}`,
      'serieAlbums',
      locale
    )
  },

  async home(subArea, locale) {
    let data

    if (subArea) {
      const params = { subArea }
      data = this.__post('/api/web/home', 'home', params, locale)
    } else {
      data = this.__get('/api/web/home', 'home', locale)
    }

    return data
  },

  async list(reference, shelf, itemId, subArea, locale) {
    return this.__post(
      '/api/web/list',
      'list',
      { reference, shelf, itemId, subArea },
      locale
    )
  },

  async getToken(sessionCookie, sessionId, locale) {
    //console.log(`getToken apiUrl='${$config.apiUrl}'`)
    try {
      const axios = new Axios.create()
      delete axios.defaults.headers.common['Authorization']
      const data = await axios.get(
        `${$config.apiUrl}/${this.__l(locale)}/api/web/user/token`,
        {
          withCredentials: true,
          transformRequest: [
            (data, headers) => {
              delete headers.common['Authorization']
              headers.common['cookie'] =
                headers.common['cookie'] +
                '; ' +
                `${sessionCookie}=${sessionId};`
              return data
            }
          ]
        }
      )
      //console.log('getToken > data=', data)

      return data.data
    } catch (e) {
      console.log(`Error retrieving user token: ${e.message}`)
    }
    return null
  },

  async addAlbumToCart(id, locale) {
    return this.__get(
      `/api/web/cart/add/${id}`,
      `/api/web/cart/add/${id}`,
      locale
    )
  },

  async removeAlbumFromCart(id, locale) {
    return this.__get(
      `/api/web/cart/remove/${id}`,
      `/api/web/cart/remove/${id}`,
      locale
    )
  },

  async addAlbumToLibrary(id, locale) {
    return this.__get(
      `/api/web/library/add/${id}`,
      `/api/web/library/add/${id}`,
      locale
    )
  },

  async removeAlbumFromLibrary(id, locale) {
    return this.__get(
      `/api/web/library/remove/${id}`,
      `/api/web/library/remove/${id}`,
      locale
    )
  },

  async followSerie(id, locale) {
    return this.__get(
      `/api/web/follow/serie/${id}`,
      `/api/web/follow/serie/${id}`,
      locale
    )
  },

  async unFollowSerie(id, locale) {
    return this.__get(
      `/api/web/unfollow/serie/${id}`,
      `/api/web/unfollow/serie/${id}`,
      locale
    )
  },

  async followUser(id, locale) {
    return this.__get(
      `/api/web/follow/user/${id}`,
      `/api/web/follow/user/${id}`,
      locale
    )
  },

  async unFollowUser(id, locale) {
    return this.__get(
      `/api/web/unfollow/user/${id}`,
      `/api/web/unfollow/user/${id}`,
      locale
    )
  },

  async getCartCount(locale) {
    return this.__get(`/api/web/cart/count`, `/api/web/cart/count`, locale)
  },

  async setAlbumRate(id, rate, locale) {
    return this.__post(
      `/api/web/album/${id}/rating`,
      `/api/web/album/${id}/rating`,
      { rate },
      locale
    )
  },

  async addSerieToCart(id, locale) {
    return this.__get(
      `/api/web/cart/serie/add/${id}`,
      `/api/web/cart/serie/add/${id}`,
      locale
    )
  },

  async removeSerieFromCart(id, locale) {
    return this.__get(
      `/api/web/cart/serie/remove/${id}`,
      `/api/web/cart/serie/remove/${id}`,
      locale
    )
  },

  async getShelvesList(locale) {
    return this.__get(`/api/web/shelf`, `/api/web/shelf`, locale)
  },

  async getGenresListByShelves(shelves, locale) {
    return this.__post(
      `/api/web/genre`,
      `/api/web/genre`,
      { shelf: shelves },
      locale
    )
  },

  async saveUserPreference(data, locale) {
    return this.__post(
      `/api/web/user/preference/save`,
      `/api/web/user/preference/save`,
      { shelves: data.shelves, genres: data.genres },
      locale
    )
  },

  async getAlbumsGift(locale) {
    return this.__get(`/api/web/albums/gift`, `/api/web/albums/gift`, locale)
  },

  async saveUserAlbumGift(id, locale) {
    return this.__get(
      `/api/web/user/album/gift/${id}`,
      `/api/web/user/album/gift/${id}`,
      locale
    )
  },

  async skipAlbumGift(locale) {
    return this.__get(
      `/api/web/user/album/gift/skip`,
      `/api/web/user/album/gift/skip`,
      locale
    )
  },

  async universe(id, locale) {
    return this.__get(`/api/web/universe/${id}`, 'universe', locale)
  },

  async universeShelfList(aboArea, locale) {
    return this.__post(
      '/api/web/universe/shelf-list',
      'universe shelf list',
      { subArea: aboArea },
      locale
    )
  },

  async topUniverses(aboArea, locale) {
    return this.__post(
      '/api/web/universe/top',
      'top universe list',
      { subArea: aboArea },
      locale
    )
  },

  async universeList(shelf, order, offset, limit, aboArea, locale) {
    return this.__post(
      `/api/web/universe/list/${shelf}/${order}/${offset}/${limit}`,
      'universe list',
      { subArea: aboArea },
      locale
    )
  },

  async getSubscriptionPacks(locale) {
    return this.__get(
      `/api/web/subscription/packs`,
      `/api/web/subscription/packs`,
      locale
    )
  },

  async getSubscriptionSection(locale) {
    return this.__get(
      `/api/web/subscription/section`,
      `/api/web/subscription/section`,
      locale
    )
  },

  async getUserData(locale) {
    return this.__get(`/api/web/user`, `/api/web/user`, locale)
  },

  async getPartnerCookieData(pId, locale) {
    return this.__get(
      `/api/web/partner?partner=${pId}`,
      `/api/web/partner`,
      locale
    )
  }
})
