import messages from './assets/locales/vuetranslations.json'

const webpack = require('webpack')
const fs = require('fs')
const path = require('path')

// const isProd = process.env.NODE_ENV === 'production'

const serverSettings =
  process.env.START_HTTPS === 1 || process.env.START_HTTPS === '1'
    ? {
        https: {
          key: fs.readFileSync(path.resolve(__dirname, 'localhost.key')),
          cert: fs.readFileSync(path.resolve(__dirname, 'localhost.crt'))
        }
      }
    : {}

export default {
  env: {
    // Izneo images (.env: IZNEO_URL)
    izneoUrl:
      process.env.IZNEO_URL || process.env.API_URL || 'https://www.izneo.com',

    // API URL (.env: API_URL)
    apiUrl: process.env.API_URL || 'https://www.izneo.com',

    gtmId: process.env.GOOGLE_TAG_MANAGER_ID,
    axeptioClientId: process.env.AXEPTIO_CLIENT_ID,

    testUser: process.env.TEST_USER,
    testPwd: process.env.TEST_PWD
  },

  // disable built-in loading component
  loading: false,

  // https must be turned off for rel2.izneo.net,
  // because there is another way for managing https (haproxy manages https)
  server: serverSettings,

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'izneo-front',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
      // {
      //   rel: 'stylesheet',
      //   type: 'text/css',
      //   href: isProd
      //     ? '/bundles/izneofront/assets/global/css/main.css'
      //     : '//www.izneo.com/bundles/izneofront/assets/global/css/main.css'
      // }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    //'~/assets/style.scss',
    //'~/assets/vuestyle.scss',
    '~/assets/font-awesome.css',
    //'~/assets/scss/v6/css/main.css',
    '~/assets/scss/v7/style.scss',
    '~/assets/toast.scss'
  ],

  styleResources: {
    scss: [
      '~/assets/scss/v7/components/settings/_variables.scss',
      '~/assets/scss/v7/components/settings/_mixins.scss'
    ]
  },

  router: {
    middleware: ['router-redirects']
  },

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/directives.js', mode: 'client' },
    { src: '~/plugins/slick-slide.js', mode: 'client' },
    { src: '~/plugins/fb-sdk.js', mode: 'client' },
    '~/plugins/config',
    '~/plugins/utils',
    '~/plugins/api',
    '~/plugins/nuxt-client-init.client.js',
    '~/plugins/gtm-init.client.js',
    '~/plugins/axeptio.client.js'
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    '@nuxt/components'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/gtm',
    '@nuxtjs/style-resources',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/toast',
    'nuxt-i18n',
    'cookie-universal-nuxt',
    'nuxt-vue-multiselect'
  ],

  i18n: {
    locales: ['fr', 'en', 'de', 'nl', 'it'],
    defaultLocale: 'fr',
    strategy: 'prefix',
    detectBrowserLanguage: {
      onlyOnRoot: true,
      useCookie: false
    },
    vueI18n: {
      messages
    },

    // Translation (https://i18n.nuxtjs.org/routing)
    parsePages: false,
    pages: {
      'subscription/index': {
        en: '/subscription',
        fr: '/abonnement',
        de: false,
        nl: false,
        it: false
      }
    }
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: process.env.API_URL || 'https://www.izneo.com',

    // this should work if CORS header "Access-Control-Allow-Origin" is not "*"
    credentials: true
  },

  auth: {
    cookie: false,
    rewriteRedirects: true,
    fullPathRedirect: true,
    redirect: {
      //login: '/login',
      logout: false,
      //callback: '/login',
      home: false
    },
    strategies: {
      local: {
        // use custom Auth header (the 'tokenName' option)
        //tokenName: 'X-Authorization',
        endpoints: {
          login: {
            url: '/fr/api/web/login',
            method: 'post',
            propertyName: 'token'
          },
          logout: false,
          user: {
            url: '/fr/api/web/user',
            method: 'get',
            propertyName: false
          }
        },
        globalToken: true,
        autoFetchUser: false
      }
    }
  },

  toast: {
    position: 'bottom-right',
    duration: 2000,
    iconPack: 'custom-class',
    theme: 'primary',
    containerClass: 'toast-notifications'
  },

  // https://github.com/nuxt-community/gtm-module
  gtm: {
    id: process.env.GOOGLE_TAG_MANAGER_ID,
    enabled: true,
    // init is called in gtm-init plugin
    autoInit: false
    //debug: true
    //crossOrigin: true
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    vendor: ['jquery'],
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery'
      })
    ]
  }
}
