export default function ({ route, app, redirect }) {
  // do some redirection before 404
  // 1. redirect /fr/subscription to /fr/abonnement
  // 2. redirect /de/subscription to home
  if (!route.matched.length) {
    if (route.path.match(/\/fr\/subscription(\/?)$/)) {
      return redirect(301, '/fr/abonnement')
    }
    if (route.path.match(/\/en\/abonnement(\/?)$/)) {
      return redirect(301, '/en/subscription')
    }
    if (
      route.path.match(/\/abonnement(\/?)$/) ||
      route.path.match(/\/subscription(\/?)$/)
    ) {
      return redirect(301, `/${app.i18n.locale}`)
    }
  }
}
