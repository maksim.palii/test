# izneo-front

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

# Docker
```bash
$ docker build -t nuxt-img .
$ docker run -d -p 3000:3000 nuxt-img
```

# Docker-compose (not tested)
```bash
$ docker-compose up -d
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
