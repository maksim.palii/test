<template>
  <div class="followers">
    <div class="followers-stack">
      <div
        v-for="(follower, index) in followers"
        v-show="index < 3"
        :key="index"
        class="avatar"
      >
        <img
          :src="`${$config.izneoUrl}/${$i18n.locale}/images/avatar/${follower.id}-24x24.jpg`"
        />
      </div>
    </div>
    <a
      v-if="followersCount === 0 && isfollowing"
      style="cursor: default !important"
    >
      {{
        $tc('serie.userfollows', followersCount, {
          count: followersCount
        })
      }}
    </a>
    <a
      v-else-if="followersCount > 0 && isfollowing"
      style="text-decoration: underline !important; cursor: pointer"
      @click="showModal = true"
    >
      {{
        $tc('serie.userfollows', followersCount, {
          count: followersCount
        })
      }}
    </a>
    <a
      v-else-if="followersCount > 0"
      style="text-decoration: underline !important; cursor: pointer"
      @click="showFollowers = true"
    >
      <span
        v-html="
          $tc('serie.otherfollow', followersCount, {
            count: followersCount
          })
        "
      ></span>
    </a>
    <a v-else style="cursor: default !important">
      <span
        v-html="
          $tc('serie.otherfollow', followersCount, {
            count: followersCount
          })
        "
      ></span>
    </a>
    <div class="info-point"></div>
    <AppModal
      v-if="showFollowers"
      :no-default="true"
      :closing-cross="true"
      :full-width="true"
      class="follower-modal"
      @close="showFollowers = false"
    >
      <template v-slot:header>
        <h2 class="text--md--home heading heading--lg text--primary text--bold">
          {{ $t('serie.followers') }}
        </h2>
      </template>
      <template v-slot:body>
        <div
          v-for="(item, index) in followers"
          :key="index"
          class="follower-item"
        >
          <div class="follower-item-info">
            <div class="follower-item-badge avatar">
              <img :src="`/${$i18n.locale}/images/avatar/${item.id}.jpg`" />
            </div>
            <div class="follower-item-name">{{ item.login }}</div>
          </div>
          <AppButton class="follower-item-follow" @click="followUser(item)">{{
            item.isfollowing ? $t('followers.unfollow') : $t('followers.follow')
          }}</AppButton>
        </div>
      </template>
    </AppModal>
  </div>
</template>

<script>
import AppModal from '@/components/ui/AppModal'
import AppButton from '@/components/ui/AppButton'

export default {
  components: { AppModal, AppButton },
  props: {
    followers: {
      type: Array,
      required: true
    },
    isfollowing: {
      type: Boolean,
      required: false,
      default: false
    },
    connected: {
      type: Boolean,
      required: false,
      default: false
    }
  },
  data() {
    return { showFollowers: false }
  },
  computed: {
    followersCount() {
      return this.followers.length
    }
  },
  methods: {
    async followUser(follower) {
      if (!this.connected) {
        const url = this.$utils.getSignupUrlForLang(this.$i18n.locale)
        window.location.href = `${this.$config.izneoUrl}/${this.$i18n.locale}/${url}?redirect=${window.location.href}`
      } else {
        let data
        if (follower.isfollowing) {
          data = await this.$api.unFollowUser(follower.id)
        } else {
          data = await this.$api.followUser(follower.id)
        }
        if (data) {
          follower.isfollowing = !follower.isfollowing
        }
      }
    }
  }
}
</script>

<style lang="scss" scoped>
.followers {
  display: flex;
  justify-content: space-between;
  align-items: center;

  a {
    font-size: 16px;
    color: #808080;
  }

  .info-point {
    margin: 0 1rem;
    width: 6px;
    height: 6px;
    min-width: 6px;
    min-height: 6px;
    background-color: #383838;
    border-radius: 99999px;
  }
}

.follower-modal {
  color: #3e3e3e;
  text-align: left;

  .follower-item {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 1.5rem;

    &:last-child {
      margin-bottom: 0;
    }

    .follower-item-info {
      display: flex;
      align-items: center;
    }

    .avatar {
      border-radius: 9999px;
      height: 36px;
      width: 36px;
      overflow: hidden;
      margin-right: 1rem;

      img {
        position: relative;
        height: 100%;
        width: 100%;
      }
    }

    .follower-item-name {
      color: #000;
      font-weight: 700;
    }

    .follower-item-follow {
      margin-left: 3rem;
      padding: 0.5rem 2rem;
    }
  }
}

.followers-stack {
  display: inline-block;
  margin-right: 18px;

  .avatar {
    width: 12px;
    height: 12px;
    position: relative;
    display: inline-block;

    img {
      position: absolute;
      left: 0;
      bottom: -6px;
      width: 24px;
      height: 24px;
      max-width: 24px;
      border-radius: 9999px;
    }
  }
}
</style>
