// Utility functions

export default (ctx, inject) => {
  const CustomUtils = {}

  // Format an album title
  CustomUtils.getTitle = (album) => {
    return (album.volume ? 'T.' + album.volume + ' - ' : '') + album.serie_name
  }

  // Format authors list
  CustomUtils.getAuthors = (album) => {
    if (!album.authors || album.authors.length === 0) return ''
    const a = []
    for (const author of album.authors) {
      a.push(author.nickname)
    }
    return a.join(' - ').substring(0, a.length - 3)
  }

  // From shelf slug to shelf id, with a promise to load shelves list
  let shelves = null
  let shelvesLoc = ''
  const locgetShelfId = (slug) => {
    // From slug to id
    for (const item of shelves) {
      if (item['slug'] == slug) return item['id']
    }
    return 0
  }
  CustomUtils.getShelfId = (loc, shelf) => {
    if (shelvesLoc != loc) shelves = null //loc has changed, need to reload shelves
    shelvesLoc = loc
    return new Promise(function (resolve) {
      if (shelves) resolve(locgetShelfId(shelf))
      return fetch(`/${loc}/api/shelves`)
        .then((res) => res.json())
        .then((data) => {
          shelves = data
          resolve(locgetShelfId(shelf))
        })
    })
  }

  CustomUtils.dictCur = {
    eur: '€',
    usd: '$',
    chf: 'SFr',
    gbp: '£',
    cad: '$',
    aud: '$',
    jpy: '¥',
    dzd: 'DA',
    mad: 'MAD'
  }

  CustomUtils.getCurrencySymbol = function (currency) {
    let short = ''
    if (currency && currency.toLowerCase() in this.dictCur)
      short = this.dictCur[currency.toLowerCase()]
    else short = currency && currency.length === 1 ? currency : ''

    return short
  }

  CustomUtils.getCurrencyBySymbol = function (symbol) {
    if (!symbol) return ''
    let prop
    for (prop in this.dictCur) {
      if (Object.prototype.hasOwnProperty.call(this.dictCur, prop)) {
        if (this.dictCur[prop].toLowerCase() === symbol.toLowerCase())
          return prop
      }
    }
    return ''
  }

  CustomUtils.getPrettyPrice = (price, currency) => {
    const short = CustomUtils.getCurrencySymbol(currency) || '€'
    const priceNumeric = price ? +price : NaN

    return isNaN(priceNumeric)
      ? ''
      : priceNumeric.toFixed(2).toString().replace('.', ',') + short
  }

  CustomUtils.generatePlayerUrl = (ean, currentHostname, exiturl) => {
    const array = currentHostname.split('.')
    array.shift()
    return (
      '//reader.' + array.join('.') + '/read/' + ean + '?exiturl=' + exiturl
    )
  }

  CustomUtils.generateNewPlayerUrl = (albumUrl, exiturl) => {
    return albumUrl + '/read' + '?exiturl=' + exiturl
  }

  CustomUtils.jQuery = () => {
    return typeof $ != 'undefined'
  }

  CustomUtils.loadSlick = (slide, size = 6, infinite = true) => {
    const settings = {
      dots: false,
      infinite: infinite,
      slidesToShow: size,
      slidesToScroll: size,
      lazyLoad: 'ondemand',
      prevArrow: '<button type="button" class="slick-prev"></button>',
      nextArrow: '<button type="button" class="slick-next"></button>',
      responsive: [
        {
          breakpoint: 1024,
          settings: 'unslick'
        }
      ]
    }

    const sl = $('.products-slider' + slide).slick(settings)

    if ($(window).width() > 1024 && !sl.hasClass('slick-initialized')) {
      $('.products-slider').slick(settings)
    }
    if ($(window).width() > 1024) {
      $('.products-slider').removeClass('products-list')
    } else {
      $('.products-slider').addClass('products-list')
    }
  }

  CustomUtils.getQueryVariable = (variable) => {
    const query = window.location.search.substring(1)
    const vars = query.split('&')
    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=')
      if (pair[0] == variable) {
        return pair[1]
      }
    }
    return false
  }

  CustomUtils.hideExternalLoader = () => {
    document.getElementById('main-vue-loader').style.display = 'none'
  }

  CustomUtils.getType = (album) => {
    return album.chapter !== null
      ? 'chapter'
      : album.volume !== null
      ? 'volume'
      : 'other'
  }

  CustomUtils.localeLanguage = (lang) => {
    switch (lang) {
      case 'fr':
        return 'fr-FR'
      case 'de':
        return 'de-DE'
      case 'nl':
        return 'nl-NL'
      case 'it':
        return 'it-IT'
      default:
        return 'en-GB'
    }
  }

  CustomUtils.scrollToTop = () => {
    window.scrollTo(0, 0)
  }

  CustomUtils.getCartUrlForLang = (lang) => {
    const langUrls = {
      fr: 'panier',
      en: 'shopping-cart',
      de: 'shopping-cart',
      nl: 'shopping-cart'
    }

    let cartUrl = langUrls['fr']
    if (lang in langUrls) {
      cartUrl = langUrls[lang]
    }

    return cartUrl
  }

  CustomUtils.prepareMetaDescription = (description) => {
    if (description.length > 250) {
      description = description.slice(0, 250)
      description =
        description.substr(
          0,
          Math.min(description.length, description.lastIndexOf(' '))
        ) + '…'
    }
    return description
  }

  CustomUtils.prettyfyText = (text) => {
    return text
      .split('-')
      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
      .join(' ')
  }

  CustomUtils.getSubscribeUrlForLang = (lang) => {
    switch (lang) {
      case 'fr':
        return 'abonnement'
      default:
        return 'subscription'
    }
  }

  CustomUtils.getSignupUrlForLang = (lang) => {
    switch (lang) {
      case 'fr':
        return 'inscription'
      default:
        return 'signup'
    }
  }

  CustomUtils.getAuthorForLang = (lang) => {
    switch (lang) {
      case 'fr':
        return 'auteur'
      default:
        return 'author'
    }
  }

  CustomUtils.getPublisherForLang = (lang) => {
    switch (lang) {
      case 'fr':
        return 'editeurs'
      default:
        return 'publisher'
    }
  }

  CustomUtils.cutByWords = function (
    text,
    toLength,
    dots = '...',
    acceptedExcess = 0.05
  ) {
    // html tags are allowed at the end only! (like '... !<br /><br />')
    if (!text.length) return ''

    const textPlain = text.replace(/<\/?[^>]+>/gi, '')
    if (textPlain.length <= toLength) return text

    // hope there is no tags...
    // TODO find nearest closing tag boundary
    let cut = text.slice(0, toLength)
    const rest = text.slice(toLength, text.length)

    // trim double spaces, trailing punctuation marks,
    // then trailing spaces
    const cutWordsNb = rest
      .replace(/<\/?[^>]+>/gi, '')
      .replace(/\s{2,}/g, ' ')
      .replace(/[.?!]*\s*$/g, '')
      .replace(/^\s*/g, '')
      .replace(/\s*$/g, '')
      .split(' ').length

    // strip tags from source and rest
    const restPlain = rest.replace(/<\/?[^>]+>/gi, '')

    if (cutWordsNb <= 1 || restPlain.length / textPlain.length < acceptedExcess)
      return text

    cut = cut.substr(0, Math.min(cut.length, cut.lastIndexOf(' '))) + dots

    return cut
  }

  CustomUtils.getPlainText = function (html) {
    if (process.client) {
      const tmp = document.createElement('div')
      tmp.innerHTML = html
      return tmp.textContent || tmp.innerText
    } else {
      return html
        .replace(/(<([^>]+)>)/gi, '')
        .replace('&amp;', '&')
        .replace(/&#[\d]+;/gi, ' ')
    }
  }

  CustomUtils.formatDate = function (strDate) {
    const d = new Date(strDate)
    return d.toLocaleDateString('en-GB', {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric'
    })
  }

  CustomUtils.gtmPush = function (event) {
    // GTM is enabled for FR only (see plugins/gtm-init.js).
    // But some functions call the $utils.gtmPush directly.
    // Skip these calls for non-FR locales. This will be changed later.
    if (process.client && this.ctx.app.i18n.locale === 'fr') {
      this.ctx.app.$gtm.push(event)
    }
  }

  CustomUtils.gtmEvent = function (event, url) {
    if (!window['google_tag_manager']) {
      if (url) window.location.href = url
      return
    }

    if (event['eventCallback'] === undefined && url) {
      event['eventCallback'] = function () {
        window.location.href = url
      }
      event['eventTimeout'] = 2000
    }

    this.gtmPush(event)
  }

  CustomUtils.gtmUpdateDataLayer = function () {
    this.gtmPush({
      country: 'FR',
      language: this.ctx.app.i18n.locale,
      visitorStatus: this.ctx.app.$auth.loggedIn ? 'loggué' : 'non loggué',
      userStatus:
        this.ctx.app.$auth.loggedIn &&
        this.ctx.app.$auth.user &&
        this.ctx.app.$auth.user.hasActiveClassicSubscription
          ? 'abonné'
          : 'non abonné',
      pageVersions:
        this.ctx.route.path.indexOf('/abo') > 0 ? 'abonné' : 'non abonné',
      pageType: 'Général'
    })
  }

  CustomUtils.gtmAdvEcommerceCreateProductImpression = function (
    name,
    id,
    price,
    brand,
    category,
    list,
    position
  ) {
    return {
      name,
      id,
      price,
      brand,
      category,
      list,
      position
    }
  }

  CustomUtils.gtmAdvEcommercePushProductImpressions = function (list) {
    if (!process.client) return

    if (list && Array.isArray(list) && list.length) {
      this.gtmEvent({
        event: 'productView',
        EventCategory: 'ecommerce',
        EventAction: 'product impression',
        ecommerce: {
          currencyCode: 'EUR',
          impressions: list
        }
      })
    }
  }

  CustomUtils.gtmAdvEcommerceCreateImpressionClickProduct = function (
    name,
    id,
    price,
    brand,
    category,
    position
  ) {
    return {
      name,
      id,
      price,
      brand,
      category,
      position
    }
  }

  CustomUtils.gtmAdvEcommerceCreateProductClickEvent = function (
    product,
    listName
  ) {
    return {
      event: 'productClick',
      EventCategory: 'ecommerce',
      EventAction: 'product click',
      ecommerce: {
        click: {
          actionField: { list: listName ?? '' },
          products: [product]
        }
      }
    }
  }

  CustomUtils.gtmAdvEcommerceCreatePromoObj = function (
    id,
    name,
    creative,
    position
  ) {
    return {
      id,
      name,
      banner: creative,
      position
    }
  }

  CustomUtils.gtmAdvEcommerceCreatePromoImpressionEvent = function (
    promotions
  ) {
    return {
      event: 'promotionView',
      EventCategory: 'ecommerce',
      EventAction: 'promotion impression',
      ecommerce: {
        promoView: {
          promotions
        }
      }
    }
  }

  CustomUtils.gtmAdvEcommerceCreatePromoClickEvent = function (promo) {
    return {
      event: 'promotionClick',
      EventCategory: 'ecommerce',
      EventAction: 'promotion click',
      ecommerce: {
        promoClick: {
          promotions: [promo]
        }
      }
    }
  }

  CustomUtils.isHomepage = function () {
    const r = new RegExp(
      `^/(${this.ctx.app.i18n.locales.join('|')})(/(abo(/)?)?)?$`
    )
    return r.test(this.ctx.route.path)
  }

  CustomUtils.aboArea = function () {
    const r = new RegExp(
      `^/(${this.ctx.app.i18n.locales.join('|')})/abo($|/$|/.+$)`
    )
    return r.test(this.ctx.route.path)
  }

  CustomUtils.storeArea = function () {
    return !CustomUtils.aboArea()
  }

  CustomUtils.homeUrlForLang = function (lang) {
    return `/${lang}`
  }

  CustomUtils.homeAboUrlForLang = function (lang) {
    return `/${lang}/abo`
  }

  CustomUtils.universeUrlForLang = function (lang) {
    return `/${lang}/universe`
  }

  CustomUtils.universeAboUrlForLang = function (lang) {
    return `/${lang}/abo/universe`
  }

  CustomUtils.nlToBr = function (v) {
    if (typeof v === 'string') {
      return v.replace(/\n/g, '<br>')
    }
    return v
  }

  CustomUtils.ctx = ctx
  inject('utils', CustomUtils)
}
