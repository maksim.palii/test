export default ({ env }, inject) => {
  const re = /^(https?:\/\/)?(www.)?([-.\w]+)((:[0-9]+)|$)/i
  const domain = re.exec(env.izneoUrl)[3]

  const config = {
    apiUrl: env.apiUrl,
    izneoUrl: env.izneoUrl,
    izneoDomain: domain
  }

  // '$config' may be in use (built-in, contains nuxt.config.js:publicRuntimeConfig data)
  inject('config', config)
}
