export default async function (store) {
  // nothing to do on server-side
  if (process.server) return true
  await store.dispatch('updateUserData')
  return true
}
