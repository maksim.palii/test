export default function ({ $gtm, env, app }) {
  // enable GTM for FR only. Will be changed later
  if (process.client && app.i18n.locale === 'fr') {
    window.dataLayer = window.dataLayer || []

    $gtm.init(env.gtmId)
  }
}
