import Vue from 'vue'

const vue_fb = {}
vue_fb.install = function install(Vue, options) {
  ;(function (d, s, id) {
    const fjs = d.getElementsByTagName(s)[0]
    if (d.getElementById(id)) {
      return
    }
    const js = d.createElement(s)
    js.id = id
    js.src = `//connect.facebook.net/${options.appFBLang}/sdk.js`
    fjs.parentNode.insertBefore(js, fjs)
  })(document, 'script', 'facebook-jssdk')

  window.fbAsyncInit = function onSDKInit() {
    // eslint-disable-next-line no-undef
    FB.init(options)
    // eslint-disable-next-line no-undef
    FB.AppEvents.logPageView()
    // eslint-disable-next-line no-undef
    Vue.FB = FB
    window.dispatchEvent(new Event('fb-sdk-ready'))
  }
  Vue.FB = undefined
}

export default ({ app }) => {
  let locale,
    version = 'v2.12',
    appId = '281241241954569'

  switch (app.i18n.locale) {
    case 'fr':
      locale = 'fr_FR'
      break
    case 'de':
      locale = 'de_DE'
      break
    case 'nl':
      locale = 'nl_NL'
      appId = '245641356184386'
      version = 'v3.0'
      break
    default:
      locale = 'en_US'
  }

  Vue.use(vue_fb, {
    appId: appId,
    autoLogAppEvents: true,
    cookie: true,
    xfbml: true,
    version: version,
    appFBLang: locale
  })
}
