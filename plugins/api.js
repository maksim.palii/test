import createApi from '~/api/api'

export default (ctx, inject) => {
  const api = createApi(ctx.app.$config, ctx.$axios, ctx.app.i18n, ctx.error)
  inject('api', api)
}
