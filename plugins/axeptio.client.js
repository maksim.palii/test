import Vue from 'vue'

const vue_axeptio = {}
vue_axeptio.install = function install(Vue, options) {
  window.axeptioSettings = {
    clientId: options.clientId
  }

  window.axeptioSettings.cookiesVersion = options.cookiesVersion
  ;(function (d, s) {
    var t = d.getElementsByTagName(s)[0],
      e = d.createElement(s)
    e.async = true
    e.src = '//static.axept.io/sdk.js'
    t.parentNode.insertBefore(e, t)
  })(document, 'script')
}

export default ({ app, env }) => {
  if (env.axeptioClientId && app.i18n.locale === 'fr') {
    const clientId = env.axeptioClientId,
      cookiesVersion = `izneo-${app.i18n.locale}`

    Vue.use(vue_axeptio, { clientId, cookiesVersion })
  }
}
